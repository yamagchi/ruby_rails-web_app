crumb :top do
  link "TOP", root_path
end

crumb :products do
   link "商品一覧",products_path
    parent :top
end
crumb :product do |product|
  link product.name,product_path(product)
  parent :products
end
#--Other--
crumb :about do
    link "紹介",about_path
    parent :top
end
crumb :about do
    link "問合せ",new_inquire_path
    parent :top
end

crumb :events do
    link "イベント記事一覧",events_path
    parent :top
end
crumb :event do |event|
  link event.title,event_path(event)
   parent :events
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).