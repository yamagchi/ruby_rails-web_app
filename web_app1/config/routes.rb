Rails.application.routes.draw do

  get 'card/new'
  get 'card/show'
  resources :orderhistories
  resources :orderlists
  resources :inquires
  get 'admin/index'
devise_for :admins, controllers: {
  sessions:      'admins/sessions',
  passwords:     'admins/passwords',
  registrations: 'admins/registrations'
}
devise_for :users, controllers: {
  sessions:      'users/sessions',
  passwords:     'users/passwords',
  registrations: 'users/registrations'
}
   get '/admin/index',to:'admin#index'
  root 'home#top'
  get '/',to: 'home#top'
  get '/about', to: 'home#about'
  
  get '/products/popular',to: 'products#popular'
  get '/products/side_dish',to: 'products#side_dish'
    get '/products/set_menu',to: 'products#set_menu'
 # post '/favorite/create',to: 'favorite#create'
 # delete '/favorite/destroy',to: 'favorite#destroy'
 # get '/favorite/index',to: 'favorite#index'
  
  post '/inquires/confirmation',to: 'inquires#confirmation'
  get '/inquires/complete',to: 'inquires#complete'
  
  get '/profiles/confirm',to: 'profiles#confirm'
  get '/profiles/:id/confirmate_delete',to: 'profiles#confirmate_delete',as: :confirmate_delete
  

  resources :carts,only:[:index,:create,:destroy]do
  collection do
    get 'destroy_all',to: 'carts#destroy_all'
  end
  end
  resources :profiles
  resources :products
  resources :inquires
  resources :admins,only:[:show]
  resources :users,only:[:show]
  resources :events
  
  resources :favorite

  
  
  resources :card, only: [:new, :show] do
  collection do
    get 'new',to: 'card#new'
    get 'orderform',to: 'card#orderform'
    post 'create', to: 'card#create'
    post 'show', to: 'card#show'
    post 'pay', to: 'card#pay'
    post 'delete', to: 'card#delete'
  end
end
end
