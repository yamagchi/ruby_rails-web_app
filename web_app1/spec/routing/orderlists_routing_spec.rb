require "rails_helper"

RSpec.describe OrderlistsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/orderlists").to route_to("orderlists#index")
    end

    it "routes to #new" do
      expect(:get => "/orderlists/new").to route_to("orderlists#new")
    end

    it "routes to #show" do
      expect(:get => "/orderlists/1").to route_to("orderlists#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/orderlists/1/edit").to route_to("orderlists#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/orderlists").to route_to("orderlists#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/orderlists/1").to route_to("orderlists#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/orderlists/1").to route_to("orderlists#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/orderlists/1").to route_to("orderlists#destroy", :id => "1")
    end
  end
end
