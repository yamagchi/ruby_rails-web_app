require "rails_helper"

RSpec.describe InquiresController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/inquires").to route_to("inquires#index")
    end

    it "routes to #new" do
      expect(:get => "/inquires/new").to route_to("inquires#new")
    end

    it "routes to #show" do
      expect(:get => "/inquires/1").to route_to("inquires#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/inquires/1/edit").to route_to("inquires#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/inquires").to route_to("inquires#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/inquires/1").to route_to("inquires#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/inquires/1").to route_to("inquires#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/inquires/1").to route_to("inquires#destroy", :id => "1")
    end
  end
end
