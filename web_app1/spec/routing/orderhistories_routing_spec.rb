require "rails_helper"

RSpec.describe OrderhistoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/orderhistories").to route_to("orderhistories#index")
    end

    it "routes to #new" do
      expect(:get => "/orderhistories/new").to route_to("orderhistories#new")
    end

    it "routes to #show" do
      expect(:get => "/orderhistories/1").to route_to("orderhistories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/orderhistories/1/edit").to route_to("orderhistories#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/orderhistories").to route_to("orderhistories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/orderhistories/1").to route_to("orderhistories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/orderhistories/1").to route_to("orderhistories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/orderhistories/1").to route_to("orderhistories#destroy", :id => "1")
    end
  end
end
