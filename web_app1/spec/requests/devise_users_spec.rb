require 'rails_helper'

RSpec.describe "UserAuthentications", type: :request do
  describe "#create" do
  
  let(:user) { attributes_for(:user)}
  let(:invaid_user){attributes_for(:user, name:"")}

    context 'パラメータが妥当な場合' do
      it 'リクエストが成功すること' do
        post user_registration_path, params: { user: user }
        expect(response.status).to eq 302
      end
  context "ユーザー名が空の場合"do
    
  it "ログインに失敗すること" do
  post user_registration_path, params: { user: invaid_user }
  expect(response.status).to eq 302
  end
end
end

end
end  
