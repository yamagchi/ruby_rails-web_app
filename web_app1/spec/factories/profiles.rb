FactoryBot.define do
  factory :profile do
    user { nil }
    last_name { "MyString" }
    first_name { "MyString" }
    prefectures { "MyString" }
    adress { "MyString" }
    municipality { "MyString" }
    building_name { "MyString" }
  end
end
