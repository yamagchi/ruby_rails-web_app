FactoryBot.define do
  factory :inquire do
    name { "MyString" }
    email { "MyString" }
    title { "MyString" }
    content { "MyText" }
  end
end
