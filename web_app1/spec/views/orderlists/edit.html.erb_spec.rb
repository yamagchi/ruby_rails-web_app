require 'rails_helper'

RSpec.describe "orderlists/edit", type: :view do
  before(:each) do
    @orderlist = assign(:orderlist, Orderlist.create!(
      :admin => nil,
      :user => nil,
      :product_items => ""
    ))
  end

  it "renders the edit orderlist form" do
    render

    assert_select "form[action=?][method=?]", orderlist_path(@orderlist), "post" do

      assert_select "input[name=?]", "orderlist[admin_id]"

      assert_select "input[name=?]", "orderlist[user_id]"

      assert_select "input[name=?]", "orderlist[product_items]"
    end
  end
end
