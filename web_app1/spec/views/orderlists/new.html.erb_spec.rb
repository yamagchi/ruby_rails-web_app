require 'rails_helper'

RSpec.describe "orderlists/new", type: :view do
  before(:each) do
    assign(:orderlist, Orderlist.new(
      :admin => nil,
      :user => nil,
      :product_items => ""
    ))
  end

  it "renders new orderlist form" do
    render

    assert_select "form[action=?][method=?]", orderlists_path, "post" do

      assert_select "input[name=?]", "orderlist[admin_id]"

      assert_select "input[name=?]", "orderlist[user_id]"

      assert_select "input[name=?]", "orderlist[product_items]"
    end
  end
end
