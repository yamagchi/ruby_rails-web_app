require 'rails_helper'

RSpec.describe "orderlists/index", type: :view do
  before(:each) do
    assign(:orderlists, [
      Orderlist.create!(
        :admin => nil,
        :user => nil,
        :product_items => ""
      ),
      Orderlist.create!(
        :admin => nil,
        :user => nil,
        :product_items => ""
      )
    ])
  end

  it "renders a list of orderlists" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
