require 'rails_helper'

RSpec.describe "inquires/index", type: :view do
  before(:each) do
    assign(:inquires, [
      Inquire.create!(
        :name => "Name",
        :email => "Email",
        :title => "Title",
        :content => "MyText"
      ),
      Inquire.create!(
        :name => "Name",
        :email => "Email",
        :title => "Title",
        :content => "MyText"
      )
    ])
  end

  it "renders a list of inquires" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
