require 'rails_helper'

RSpec.describe "inquires/show", type: :view do
  before(:each) do
    @inquire = assign(:inquire, Inquire.create!(
      :name => "Name",
      :email => "Email",
      :title => "Title",
      :content => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
  end
end
