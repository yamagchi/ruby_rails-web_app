require 'rails_helper'

RSpec.describe "inquires/edit", type: :view do
  before(:each) do
    @inquire = assign(:inquire, Inquire.create!(
      :name => "MyString",
      :email => "MyString",
      :title => "MyString",
      :content => "MyText"
    ))
  end

  it "renders the edit inquire form" do
    render

    assert_select "form[action=?][method=?]", inquire_path(@inquire), "post" do

      assert_select "input[name=?]", "inquire[name]"

      assert_select "input[name=?]", "inquire[email]"

      assert_select "input[name=?]", "inquire[title]"

      assert_select "textarea[name=?]", "inquire[content]"
    end
  end
end
