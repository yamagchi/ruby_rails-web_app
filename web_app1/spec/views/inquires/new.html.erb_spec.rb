require 'rails_helper'

RSpec.describe "inquires/new", type: :view do
  before(:each) do
    assign(:inquire, Inquire.new(
      :name => "MyString",
      :email => "MyString",
      :title => "MyString",
      :content => "MyText"
    ))
  end

  it "renders new inquire form" do
    render

    assert_select "form[action=?][method=?]", inquires_path, "post" do

      assert_select "input[name=?]", "inquire[name]"

      assert_select "input[name=?]", "inquire[email]"

      assert_select "input[name=?]", "inquire[title]"

      assert_select "textarea[name=?]", "inquire[content]"
    end
  end
end
