require 'rails_helper'

RSpec.describe "products/edit", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :name => "MyText",
      :product_deta => "MyText",
      :price => ""
    ))
  end

  it "renders the edit product form" do
    render

    assert_select "form[action=?][method=?]", product_path(@product), "post" do

      assert_select "textarea[name=?]", "product[name]"

      assert_select "textarea[name=?]", "product[product_deta]"

      assert_select "input[name=?]", "product[price]"
    end
  end
end
