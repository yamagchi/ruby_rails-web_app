require 'rails_helper'

RSpec.describe "products/new", type: :view do
  before(:each) do
    assign(:product, Product.new(
      :name => "MyText",
      :product_deta => "MyText",
      :price => ""
    ))
  end

  it "renders new product form" do
    render

    assert_select "form[action=?][method=?]", products_path, "post" do

      assert_select "textarea[name=?]", "product[name]"

      assert_select "textarea[name=?]", "product[product_deta]"

      assert_select "input[name=?]", "product[price]"
    end
  end
end
