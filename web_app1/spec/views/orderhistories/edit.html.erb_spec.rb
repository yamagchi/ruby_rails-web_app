require 'rails_helper'

RSpec.describe "orderhistories/edit", type: :view do
  before(:each) do
    @orderhistory = assign(:orderhistory, Orderhistory.create!(
      :user => nil,
      :product_items => "",
      :status => "MyString"
    ))
  end

  it "renders the edit orderhistory form" do
    render

    assert_select "form[action=?][method=?]", orderhistory_path(@orderhistory), "post" do

      assert_select "input[name=?]", "orderhistory[user_id]"

      assert_select "input[name=?]", "orderhistory[product_items]"

      assert_select "input[name=?]", "orderhistory[status]"
    end
  end
end
