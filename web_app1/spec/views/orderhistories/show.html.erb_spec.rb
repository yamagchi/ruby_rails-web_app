require 'rails_helper'

RSpec.describe "orderhistories/show", type: :view do
  before(:each) do
    @orderhistory = assign(:orderhistory, Orderhistory.create!(
      :user => nil,
      :product_items => "",
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Status/)
  end
end
