require 'rails_helper'

RSpec.describe "orderhistories/index", type: :view do
  before(:each) do
    assign(:orderhistories, [
      Orderhistory.create!(
        :user => nil,
        :product_items => "",
        :status => "Status"
      ),
      Orderhistory.create!(
        :user => nil,
        :product_items => "",
        :status => "Status"
      )
    ])
  end

  it "renders a list of orderhistories" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
