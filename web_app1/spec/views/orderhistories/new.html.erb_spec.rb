require 'rails_helper'

RSpec.describe "orderhistories/new", type: :view do
  before(:each) do
    assign(:orderhistory, Orderhistory.new(
      :user => nil,
      :product_items => "",
      :status => "MyString"
    ))
  end

  it "renders new orderhistory form" do
    render

    assert_select "form[action=?][method=?]", orderhistories_path, "post" do

      assert_select "input[name=?]", "orderhistory[user_id]"

      assert_select "input[name=?]", "orderhistory[product_items]"

      assert_select "input[name=?]", "orderhistory[status]"
    end
  end
end
