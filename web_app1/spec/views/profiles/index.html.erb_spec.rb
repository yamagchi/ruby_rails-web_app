require 'rails_helper'

RSpec.describe "profiles/index", type: :view do
  before(:each) do
    assign(:profiles, [
      Profile.create!(
        :user => nil,
        :last_name => "Last Name",
        :first_name => "First Name",
        :prefectures => "Prefectures",
        :adress => "Adress",
        :municipality => "Municipality",
        :building_name => "Building Name"
      ),
      Profile.create!(
        :user => nil,
        :last_name => "Last Name",
        :first_name => "First Name",
        :prefectures => "Prefectures",
        :adress => "Adress",
        :municipality => "Municipality",
        :building_name => "Building Name"
      )
    ])
  end

  it "renders a list of profiles" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Prefectures".to_s, :count => 2
    assert_select "tr>td", :text => "Adress".to_s, :count => 2
    assert_select "tr>td", :text => "Municipality".to_s, :count => 2
    assert_select "tr>td", :text => "Building Name".to_s, :count => 2
  end
end
