require 'rails_helper'

RSpec.describe "profiles/edit", type: :view do
  before(:each) do
    @profile = assign(:profile, Profile.create!(
      :user => nil,
      :last_name => "MyString",
      :first_name => "MyString",
      :prefectures => "MyString",
      :adress => "MyString",
      :municipality => "MyString",
      :building_name => "MyString"
    ))
  end

  it "renders the edit profile form" do
    render

    assert_select "form[action=?][method=?]", profile_path(@profile), "post" do

      assert_select "input[name=?]", "profile[user_id]"

      assert_select "input[name=?]", "profile[last_name]"

      assert_select "input[name=?]", "profile[first_name]"

      assert_select "input[name=?]", "profile[prefectures]"

      assert_select "input[name=?]", "profile[adress]"

      assert_select "input[name=?]", "profile[municipality]"

      assert_select "input[name=?]", "profile[building_name]"
    end
  end
end
