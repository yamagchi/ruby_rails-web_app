require 'rails_helper'

RSpec.describe "profiles/new", type: :view do
  before(:each) do
    assign(:profile, Profile.new(
      :user => nil,
      :last_name => "MyString",
      :first_name => "MyString",
      :prefectures => "MyString",
      :adress => "MyString",
      :municipality => "MyString",
      :building_name => "MyString"
    ))
  end

  it "renders new profile form" do
    render

    assert_select "form[action=?][method=?]", profiles_path, "post" do

      assert_select "input[name=?]", "profile[user_id]"

      assert_select "input[name=?]", "profile[last_name]"

      assert_select "input[name=?]", "profile[first_name]"

      assert_select "input[name=?]", "profile[prefectures]"

      assert_select "input[name=?]", "profile[adress]"

      assert_select "input[name=?]", "profile[municipality]"

      assert_select "input[name=?]", "profile[building_name]"
    end
  end
end
