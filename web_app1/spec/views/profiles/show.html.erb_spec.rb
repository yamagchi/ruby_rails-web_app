require 'rails_helper'

RSpec.describe "profiles/show", type: :view do
  before(:each) do
    @profile = assign(:profile, Profile.create!(
      :user => nil,
      :last_name => "Last Name",
      :first_name => "First Name",
      :prefectures => "Prefectures",
      :adress => "Adress",
      :municipality => "Municipality",
      :building_name => "Building Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Prefectures/)
    expect(rendered).to match(/Adress/)
    expect(rendered).to match(/Municipality/)
    expect(rendered).to match(/Building Name/)
  end
end
