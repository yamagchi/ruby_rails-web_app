class RemoveAdminIdFromEventposts < ActiveRecord::Migration[5.2]
  def change
    remove_column :eventposts, :admin_id, :bigint
  end
end
