class AddReferencesToEventposts < ActiveRecord::Migration[5.2]
  def change
        add_reference :eventposts, :admin, null: false, foreign_key: true

  end
end
