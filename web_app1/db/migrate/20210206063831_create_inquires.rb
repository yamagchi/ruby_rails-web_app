class CreateInquires < ActiveRecord::Migration[5.2]
  def change
    create_table :inquires do |t|
      t.string :name
      t.string :email
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
