class CreateOrderlists < ActiveRecord::Migration[5.2]
  def change
    create_table :orderlists do |t|
      t.references :admin, foreign_key: true
      t.references :user, foreign_key: true
      t.json :product_items

      t.timestamps
    end
  end
end
