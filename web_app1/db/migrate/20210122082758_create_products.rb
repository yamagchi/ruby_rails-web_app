class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.text :name
      t.text :product_deta
      t.integer :price
      t.string :limited
      t.timestamps
    end
  end
end
