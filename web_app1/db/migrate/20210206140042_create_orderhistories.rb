class CreateOrderhistories < ActiveRecord::Migration[5.2]
  def change
    create_table :orderhistories do |t|
      t.references :user, foreign_key: true
      t.json :product_items
      t.string :status

      t.timestamps
    end
  end
end
