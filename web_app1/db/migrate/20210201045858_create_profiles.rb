class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :last_name
      t.string :first_name
      t.string :prefectures
      t.string :adress
      t.string :municipality
      t.string :building_name

      t.timestamps
    end
  end
end
