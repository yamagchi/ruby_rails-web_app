class User < ApplicationRecord
    has_many :profiles,dependent: :destroy
    has_many :carts,dependent: :destroy
    has_many :favorites,dependent: :destroy
    has_many :orderhistories,dependent: :destroy
    has_many :cards,dependent: :destroy

    has_many :products, through: :favorites
    has_many :products,through: :carts
    #has_many :products,through: :orderhistories
    
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
         
         VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
         VALID_PASSWORD_REGEX = /\A(?=.*?[a-z])(?=.*?\d)[a-z\d]+\z/i
         validates :name, presence: true, length: { maximum: 50 }
         validates :email, presence: true, uniqueness: true,length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }
         validates :password, presence: true, length: { minimum: 7 }, format: { with: VALID_PASSWORD_REGEX }
end
