class Product < ApplicationRecord
    mount_uploaders :image, ImagesUploader
    acts_as_taggable
    has_many :favorites, dependent: :destroy
    has_many :cart, dependent: :destroy
    has_many :users, through: :favorites
    has_many :users,through: :carts

end
