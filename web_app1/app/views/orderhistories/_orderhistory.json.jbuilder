json.extract! orderhistory, :id, :user_id, :product_items, :status, :created_at, :updated_at
json.url orderhistory_url(orderhistory, format: :json)
