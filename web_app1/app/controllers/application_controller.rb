class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user
  helper_method :current_admin
  def configure_permitted_parameters
    added_attrs = [ :email,:name,:password, :password_confirmation ]
    devise_parameter_sanitizer.permitkeys:sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: added_attrs

  end
  
  def after_sign_in_path_for(resource)

    if current_admin
       #admin?sign_in
       return admin_index_path
    elsif current_user

       #sign_in
       return root_path
    else
       #sign_up
       return profiles_confirm_path  
    end
      
 
  end

end
