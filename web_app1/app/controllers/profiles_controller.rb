class ProfilesController < ApplicationController
  before_action :set_profile, only: [:index,:show, :edit, :update, :destroy,:confirm_delete]

  # GET /profiles
  def index
  @user =User.find_by(params[:id])
  
  end

  # GET /profiles/1
  def show

  end

  # GET /profiles/new
  def new
   # @user= User.find_by(user_id: current_user.id)
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  def create
    if !Profile.find_by(user_id: current_user.id) 
      
      @profile= current_user.profiles.create(profile_params)
    end
    
    (@profile.save!)? (render "complete") : (redirect_to new_profile_path)
  end

  # PATCH/PUT /profiles/1
  def update
   @profile = current_user.profiles.update(profile_params)
    if @profile
      flash[:notice] = "プロフィールを編集しました"
      redirect_to profile_path(@profile)
    else
      render "edit"
  
    end
  end
  def confirm
    
  end
  def confirmate_delete

  end
  def complete
    
  end
  
  def destroy
   @user= User.find_by(id: current_user.id)
    
   @profile.destroy
    @user.destroy
  redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find_by(user_id: current_user.id)
    end

    # Only allow a list of trusted parameters through.
    def profile_params
      params.require(:profile).permit(:user_id, :last_name, :first_name, :prefectures, :adress, :municipality, :building_name)
    end
end
