class FavoriteController < ApplicationController

  def index

  @user=User.find_by(params[:id])
  @favorites=Favorite.where(user_id: current_user.id).page(params[:page])

  end
  
  def create
  #   binding.pry
  @product = Product.find_by(id: params[:id])
      if Favorite.exists?(user_id: current_user.id, product_id: @product.id)
       redirect_to product_path(@product.id)
     else
       @favorite = current_user.favorites.create(product_id: @product.id)
      end
      
       redirect_to product_path(@product.id)
     
     
  end

  def destroy
     @product=Product.find(params[:id])
     @favorite = Favorite.find_by(user_id: current_user.id,product_id: @product.id)
      @favorite.destroy
     redirect_back(fallback_location: root_path)
  end
  
private

 

end
