class OrderlistsController < ApplicationController
  before_action :set_orderlist, only: [:show, :edit, :update, :destroy]

  # GET /orderlists
  # GET /orderlists.json
  def index
    
    @orderlists = Orderlist.all
  end

  # GET /orderlists/1
  # GET /orderlists/1.json
  def show
  end

  # GET /orderlists/new
  def new
    @orderlist = Orderlist.new
  end

  # GET /orderlists/1/edit
  def edit
  end

  # POST /orderlists
  # POST /orderlists.json
  def create
    @orderlist = Orderlist.new(orderlist_params)

    respond_to do |format|
      if @orderlist.save
        format.html { redirect_to @orderlist, notice: 'Orderlist was successfully created.' }
        format.json { render :show, status: :created, location: @orderlist }
      else
        format.html { render :new }
        format.json { render json: @orderlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orderlists/1
  # PATCH/PUT /orderlists/1.json
  def update
    respond_to do |format|
      if @orderlist.update(orderlist_params)
        format.html { redirect_to @orderlist, notice: 'Orderlist was successfully updated.' }
        format.json { render :show, status: :ok, location: @orderlist }
      else
        format.html { render :edit }
        format.json { render json: @orderlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orderlists/1
  # DELETE /orderlists/1.json
  def destroy
    @orderlist.destroy
    respond_to do |format|
      format.html { redirect_to orderlists_url, notice: 'Orderlist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orderlist
      @orderlist = Orderlist.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def orderlist_params
      params.require(:orderlist).permit(:admin_id, :user_id, :product_items)
    end
end
