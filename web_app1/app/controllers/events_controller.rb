class EventsController < ApplicationController
    before_action :find_admin, only: [:show, :edit, :update, :destroy]

  
    def index
        @eventposts = Eventpost.all.page(params[:page])
       
    end
    
    def show
      
    end
    
    def new
       @eventpost = Eventpost.new 
       
    end
    
def create
     @eventpost = current_admin.eventpost.build(eventpost_params)
     @eventpost.save
    redirect_to events_path
end
  
def edit

  if @eventpost.admin == current_admin
      render "edit"
  else
      redirect_to events_path
      flash[:notice]="エラー：ユーザーが一致しません"
  end
end

def update
    @eventpost.update(eventpost_params)
   
    if @eventpost.save!
      flash[:notice] = "投稿を編集しました"
      redirect_to events_path
    else
      render "edit"
  
    end
end
  
def destroy
    if @eventpost.admin == current_admin
        @eventpost.destroy
        flash[:notice] = "投稿を削除しました"
        redirect_to events_path
    else
        redirect_to events_path
        flash[:notice]="エラー：ユーザーが一致しません"
    end
end
  
  private
  
def find_admin
      @eventpost = Eventpost.find_by(id: params[:id])
end  

def eventpost_params
     params.require(:eventpost).permit(:title,:content) 
end
  
 
end
