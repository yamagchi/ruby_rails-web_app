class CardController < ApplicationController

  require "payjp"

  def new
    @card = Card.new
  end
  
  def orderform
  @orderlist= Orderlist.new
  end
  
  def pay
   # binding.pry
   Payjp.api_key = ENV['PAYJP_PRIVATE_KEY']
   @user= User.find_by(id: current_user.id)
   @carts= Cart.where(user_id: current_user.id)
  charge = Payjp::Charge.create(
  :amount => 3500,
  :card => params['payjp-token'],
  :currency => 'jpy',
)
    
   @orderlist=Orderlist.create(admin_id:1,user_id:current_user.id,product_items:@carts) 
   redirect_to destroy_all_carts_path
   
  end
  
  def show 
    
    @card = Card.where(user_id: current_user.id)
    
    if @card.blank?
      redirect_to action: "new" 
    else
      Payjp.api_key = ENV['PAYJP_PRIVATE_KEY']
      customer = Payjp::Customer.retrieve(@card.customer_id)
      @default_card_information = customer.cards.retrieve(@card.card_id)
    end
   
  end

  def create
    Payjp.api_key = ENV['PAYJP_PRIVATE_KEY']
    puts ENV['PAYJP_PRIVATE_KEY']
    puts'------'
    puts params['payjp_token']
    puts'------'
    if params['payjp_token'].blank?
      
      render "new"
    else
      customer = Payjp::Customer.create(
      description: '登録テスト', 
      email: current_user.email, 
      card: params['payjp_token'],
      metadata: {user_id: current_user.id}
      ) 
      @card = Card.new(user_id: current_user.id, customer_id: customer.id, card_id: customer.default_card)
      if @card.save!
        redirect_to action: "show"
      else
        redirect_to action: "new"
      end
    end
  end

  def delete
    @card = Card.where(user_id: current_user.id).first 
    
    if @card.blank?
    else
      Payjp.api_key = ENV['PAYJP_PRIVATE_KEY']
      customer = Payjp::Customer.retrieve(card.customer_id)
      customer.delete
      @card.delete
    end
      redirect_to action: "new"
  end
  
end