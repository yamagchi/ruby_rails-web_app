class CartsController < ApplicationController


  def index
    
    if !user_signed_in?
      redirect_to new_user_session_path
    else
      @user=User.find_by(params[:id])
      @carts= Cart.where(user_id: current_user.id).page(params[:page])
      
    end
  end
  
  def create
    if !user_signed_in?
      redirect_to new_user_session_path and return
    end
    
    @product = Product.find_by(id: params[:id])

     if Cart.exists?(user_id: current_user.id, product_id: @product.id)
       redirect_to product_path(@product.id)and return
     else
       @cart = current_user.carts.create(product_id: @product.id,quantity:1)
     end
      
       redirect_back(fallback_location: root_path)and return
  end

  def destroy
    @product=Product.find(params[:id])
    @cart = Cart.find_by(user_id: current_user.id,product_id: @product.id)
      @cart.destroy
      redirect_back(fallback_location: root_path)
  end
  def destroy_all
    @carts= Cart.where(user_id: current_user.id).destroy_all
    redirect_to root_path
  end

  
end
