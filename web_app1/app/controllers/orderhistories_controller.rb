class OrderhistoriesController < ApplicationController
  before_action :set_orderhistory, only: [:show,:destroy]

  # GET /orderhistories
  # GET /orderhistories.json
  def index
    @orderhistories = Orderhistory.all
  end

  # GET /orderhistories/1
  # GET /orderhistories/1.json
  def show
  end

  # GET /orderhistories/new
  def new
    @orderhistory = Orderhistory.new
  end

  # GET /orderhistories/1/edit
  def edit
  end

  # POST /orderhistories
  # POST /orderhistories.json
  def create
    @orderhistory = Orderhistory.new(orderhistory_params)

    respond_to do |format|
      if @orderhistory.save
        format.html { redirect_to @orderhistory, notice: 'Orderhistory was successfully created.' }
        format.json { render :show, status: :created, location: @orderhistory }
      else
        format.html { render :new }
        format.json { render json: @orderhistory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orderhistories/1
  # PATCH/PUT /orderhistories/1.json
  def update
    respond_to do |format|
      if @orderhistory.update(orderhistory_params)
        format.html { redirect_to @orderhistory, notice: 'Orderhistory was successfully updated.' }
        format.json { render :show, status: :ok, location: @orderhistory }
      else
        format.html { render :edit }
        format.json { render json: @orderhistory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orderhistories/1
  # DELETE /orderhistories/1.json
  def destroy
    @orderhistory.destroy
    respond_to do |format|
      format.html { redirect_to orderhistories_url, notice: 'Orderhistory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orderhistory
      @orderhistory = Orderhistory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def orderhistory_params
      params.require(:orderhistory).permit(:user_id, :product_items, :status)
    end
end
