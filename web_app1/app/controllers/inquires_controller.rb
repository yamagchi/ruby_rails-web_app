class InquiresController < ApplicationController


  # GET /inquires
  def index
    @inquires = Inquire.all
  end

  # GET /inquires/1
  def show
        @inquire = Inquire.new(inquire_params)

  end
  
  def confirmation
    @inquire = Inquire.new(inquire_params)
    #return if @inquire.valid?

  #  render :new
  end

  # GET /inquires/new
  def new
    @inquire = Inquire.new
  end

  # GET /inquires/1/edit
  #def edit
  #end

  # POST /inquires
  def create
   if params[:back]
      @inquire = Inquire.new(inquire_params)
      return render :new
   else    
      @inquire = Inquire.create(inquire_params)
   end
  end
  # PATCH/PUT /inquires/1
 # def update
  #end

  # DELETE /inquires/1
  def destroy
    @inquire = Inquire.new(inquire_params)
    @inquire.destroy
  end

  private

    def inquire_params
      params.require(:inquire).permit(:name, :email, :title, :content)
    end
end
