class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  def index
    #binding.pry
    @products = Product.all.page(params[:page])
  end

  def popular
    @products = Product.all.page(params[:page])
  end

  def side_dish
       @products = Product.all.tagged_with(["惣菜","side_dish"],any: true).page(params[:page])

  end
  
  def set_menu
       @products = Product.all.tagged_with(["セット","セットメニュー","set"],any: true).page(params[:page])
  end
  # GET /products/1
  def show
 
puts @product.id
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
  @product.save
  redirect_to products_path
  end

  # PATCH/PUT /products/1
  def update
   @product.update(product_params)
    if @product.save!
     flash[:notice] = "投稿を編集しました"
     redirect_to products_path
    else
     render "edit"
  
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  redirect_to products_path
  end

 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find_by(id: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def product_params
      params.require(:product).permit(:name, :product_deta, :price,{image: []}, :tag_list)
    end
end
